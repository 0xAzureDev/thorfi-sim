from dataclasses import dataclass


@dataclass
class pool:
    pool_asset_depth: float
    pool_rune_depth: float
    pool_asset_collateral: float
    pool_rune_collateral: float
    pool_yield: float  # percent
    asset_price_usd: float


@dataclass
class Thorchad:
    pool: str
    lp_asset_amount: float  # portion of Lp in asset term
    lp_rune_amount: float  # portion of lp in rune term
    debt_owed: float  # in thor.usd terms
    debt_received: float  # in thor.usd terms
    CR: float  # collateralization ratio
    savings_asset: str  # derrived asset
    savings_deposit: str  # in asset units, i.e. - thor.btc


@dataclass
class RuneSupply:
    rune_supply: float  # total rune supply
    rune_lend_inflation: float
    rune_save_deflation: float


@dataclass
class LendModule:
    asset: str
    debt_minted: float
    debt_owed: float


@dataclass
class ThorUsdVault:
    fee_revenue: float
    deposits: float
    withdrawals: float
