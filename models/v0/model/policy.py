

# - todo move these constants to a config yaml


RUNE_PRICE_USD = 7.0
POOL_AMP_FACTOR = 1.0
# Loan MaxCR to tweak attracting all lenders (down to get more lenders)
MAX_CR = 3.0  # in percent, 3 = 300%
###
# Helpers
###


def calc_collat_ratio(P, C, uC, maxCR):
    '''
    P = total rune in pool (pooled rune)
    C = total rune in pool as loan collateral (collateral rune)
    uC = user rune collateral
    maxCR = maximum collateralization ratio
    '''
    CR = ((C + uC)/P * (maxCR-1.0)) + 1.0

    return CR


def lend(r, a, R, A, Rv, Uv):
    """
    r: rune deposit value
    a: asset deposit value
    R: rune depth in pool
    A: asset depth in pool
    Rv: virtual rune depth
    Uv: virtual  USD depth

    note - pool amp factor will scale the debt minted. 
            max value of 1, lower value will increase fee to thor.usd vault

    returns: 
        d: total deposit value in rune
        u: debt minted for user, THOR.USD - this is the amount OWED to the protocol to get collat back.
    """

    d = r + (R/A * a)
    u = (d * Rv * Uv)/(d + Rv) ** 2 * POOL_AMP_FACTOR

    return u  # this is the value owed to the network


###
# Lend Policy
###

def lend_policy(_params, substep, state_history, previous_state):
    """
    Takes users that indicate they want to borrow.
    Calculates all the values to pass to state_update fucntions as policy_input

    lend policy is used within a partial update block (calculations that happen within a block)
    """

    # TODO - how can I have a switch key to pay back or not...

    # not sure how to iterate users
    user = previous_state['users']['user1']
    btc_pool = previous_state['btc_pool']
    thor_usd_pool = previous_state['thor_usd_pool']

    # Specify the LP position components
    rune_collat = user.lp_rune_amount  # half of LP position value
    btc_collat = user.lp_asset_amount  # other half of LP position value

    collat_usd_value = (rune_collat * RUNE_PRICE_USD) + \
        (btc_collat * btc_pool.asset_price_usd)

    # get CR
    lend_cr = calc_collat_ratio(
        P=btc_pool.pool_rune_depth,
        C=btc_pool.pool_rune_collateral,
        uC=rune_collat,
        maxCR=MAX_CR)

    # Get portion of the collateral protocol is willing to lend
    rune_lend = rune_collat / lend_cr
    btc_lend = btc_collat / lend_cr

    debt_owed = (rune_lend * RUNE_PRICE_USD) + \
        (btc_lend * btc_pool.asset_price_usd)

    debt_minted = lend(
        r=rune_lend,  # in rune terms
        a=btc_lend,  # in asset terms
        R=btc_pool.pool_rune_depth,
        A=btc_pool.pool_asset_depth,
        Rv=thor_usd_pool.pool_rune_depth,  # RUNE in THOR.USD/RUNE pool
        Uv=thor_usd_pool.pool_asset_depth,  # THOR.USD in THOR.USD/RUNE pool
    )

    # increase users debt owed, recived
    # increase the lend module blances
    # increase portion of the pool that is collateral
    # increase rune supply

    policy_update_values = {
        'update_user_debt_owed': debt_owed,  # in thor.usd terms
        'update_user_debt_received': debt_minted,  # in thor.usd terms
        'update_user_cr': lend_cr,  # user's collat ratio
        'update_pool_debt_minted': btc_lend_module.debt_minted + debt_minted,
        'update_pool_debt_owed': btc_lend_module.debt_owed + debt_owed,
        'update_pool_rune_collateral': btc_pool.pool_rune_collateral + rune_collat,
        'update_pool_asset_collateral': btc_pool.pool_asset_collateral + btc_collat,
        'update_rune_lend_inflation': rune_supply_tracker.rune_lend_inflation + (debt_owed / RUNE_PRICE_USD),
        'update_thor_usd_fee_revenue': usd_vault.fee_revenue + np.round(debt_owed - debt_minted,  5)

    }

    return (policy_update_values)
