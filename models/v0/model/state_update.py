from typing import Tuple


def su_btc_pool(params, step, sL, s, _input) -> Tuple:
    """
    state update for btc pool
    """

    new_state = s.copy()
    btc_pool = new_state['btc_pool']

    btc_pool.pool_rune_collateral = _input['update_pool_rune_collateral']
    btc_pool.pool_asset_collateral = _input['update_pool_asset_collateral']

    return ('btc_pool', btc_pool)


def su_thor_usd_pool(params, step, sL, s, _input) -> Tuple:
    """
    state update for thor.usd pool
    """

    new_state = s.copy()

    # TODO - no updates here yet

    return ('thor_usd_pool', new_state['thor_usd_pool'])


####
# Lend Module - state update function
####

def su_lend_module(params, step, sL, s, _input) -> Tuple:
    """
    State update for lend module

    Updates lend_module state for BTC lend module
    """

    new_state = s.copy()
    lend_module = new_state['lend_module']

    lend_module.debt_minted = _input['update_pool_debt_minted']
    lend_module.debt_owed = _input['update_pool_debt_owed']

    return ('lend_module', lend_module)


def su_rune_supply_tracker(params, step, sL, s, _input) -> Tuple:
    """
    state update for rune supply tracker
    """
    new_state = s.copy()
    rune_supply_tracker = new_state['rune_supply_tracker']

    rune_supply_tracker.rune_lend_inflation = _input['update_rune_lend_inflation']

#     if policy_input['update_rune_save_deflation']:
#         new_state['rune_supply_tracker']['rune_save_deflation'] = policy_input['update_rune_save_deflation']
#         new_state['rune_supply_tracker']['rune_supply'] = inital_supply + policy_input['update_rune_lend_inflation'] - policy_input['update_rune_save_deflation']

#     else:
    rune_supply_tracker.rune_supply = rune_supply_tracker.rune_supply + \
        _input['update_rune_lend_inflation']

    return ('rune_supply_tracker', rune_supply_tracker)


def su_thor_usd_vault(params, step, sL, s, _input) -> Tuple:
    """
    state update for thor.usd Vault
    """
    new_state = s.copy()
    thor_usd_vault = new_state['thor_usd_vault']
    # += dangerous here?
    thor_usd_vault.fee_revenue += _input['update_thor_usd_fee_revenue']

    return ('thor_usd_vault', thor_usd_vault)


# increase users debt owed, recived
def su_users(params, step, sL, s, _input) -> Tuple:
    """
    Update user state
    """

    # NOTE - single user

    new_state = s.copy()
    users = new_state['users']

    # increase owed, recived in thor.usd terms
    users['user1'].debt_owed = _input['update_user_debt_owed']
    users['user1'].debt_received = _input['update_user_debt_received']
    users['user1'].CR = _input['update_user_cr']

    return('users', users)
