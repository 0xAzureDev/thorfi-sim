import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
from plotly.subplots import make_subplots
import plotly.graph_objects as go


def _user_CR_plot(result_df: pd.DataFrame):

    cr_list = []
    for step in result_df["users"]:

        # TODO - this handles a single user!
        if step['user1'].CR > 0:
            cr_list.append(step["user1"].CR)

    fig = make_subplots(
        rows=1, cols=1,
    )
    # skip block 1 as that begins at 0
    # hacky but we dont want a 0 value,
    # we do this so that CR at blocks are aligned correctly
    cr_list[0] = cr_list[1]

    fig.add_trace(go.Scatter(y=cr_list,  name='Collat. Ratio'),
                  row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="C.R.", row=1, col=1)

    fig.update_layout(height=600, width=800,
                      title_text="Lending - Protocol Collateralization Ratio")
    fig.show()


def _btc_pool_plot(result_df: pd.DataFrame):

    asset_collat = []
    rune_collat = []

    for step in result_df["btc_pool"]:

        asset_collat.append(step.pool_asset_collateral)
        rune_collat.append(step.pool_rune_collateral)

    fig = make_subplots(
        rows=2,
        cols=1,
        # specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        subplot_titles=("Asset Collateral", "Rune Collateral"),
    )

    # Asset collateral
    fig.add_trace(go.Scatter(y=asset_collat, name="BTC"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="BTC", row=1, col=1)
    # rune collateral
    fig.add_trace(go.Scatter(y=rune_collat, name="Rune"), row=2, col=1)
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="Rune", row=2, col=1)

    fig.update_layout(height=500, width=800, title_text="Lending - BTC POOL")

    fig.show()


def _thor_usd_vault_plot(result_df: pd.DataFrame):

    fee_revenue = []
    deposits = []
    withdrawals = []

    for step in result_df["thor_usd_vault"]:

        fee_revenue.append(step.fee_revenue)
        deposits.append(step.deposits)
        withdrawals.append(step.withdrawals)

    fig = make_subplots(
        rows=3,
        cols=1,
        # specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        vertical_spacing=0.25,
        subplot_titles=(
            "Fee Revenue - THOR.USD Savings Vault",
            "Deposits - THOR.USD Savings Vault",
            "Withdrawals - THOR.USD Savings Vault",
        ),
    )

    # Fee Revenue
    fig.add_trace(go.Scatter(y=fee_revenue, name="Fee Revenue"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=1, col=1)

    # Deposits
    fig.add_trace(go.Scatter(
        y=deposits, name="Deposits (THOR.USD)"), row=2, col=1)
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=2, col=1)

    # Withdrawals
    fig.add_trace(
        go.Scatter(y=withdrawals, name="Withdrawals (THOR.USD)"), row=3, col=1
    )
    fig.update_xaxes(title_text="Step (block)", row=3, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=3, col=1)

    fig.update_layout(height=800, width=800,
                      title_text="THOR.USD Savings Vault")

    fig.show()


def _lend_module_plot(result_df: pd.DataFrame):

    debt_owed = []
    debt_minted = []

    for step in result_df["lend_module"]:

        debt_owed.append(step.debt_owed)
        debt_minted.append(step.debt_owed)

    fig = make_subplots(
        rows=2,
        cols=1,
        # specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        vertical_spacing=0.25,
        subplot_titles=(
            "Debt Owed (THOR.USD)",
            "Debt Minted (THOR.USD)",
        ),
    )

    # debt_owed
    fig.add_trace(go.Scatter(
        y=debt_owed, name="Debt Owed (THOR.USD)"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=1, col=1)

    # Debt Minted
    fig.add_trace(
        go.Scatter(y=debt_minted, name="Debt Minted (THOR.USD)"), row=2, col=1
    )
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=2, col=1)

    fig.update_layout(height=500, width=800,
                      title_text="Lend Module Debt Balances")

    fig.show()


def _thor_usd_pool_plot(result_df: pd.DataFrame):

    rune_price = []

    for step in result_df["thor_usd_pool"]:

        rune_price.append(step.asset_price_usd)

    fig = make_subplots(rows=1, cols=1)

    fig.add_trace(go.Scatter(y=rune_price, name="Price, USD"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="USD ($)", row=1, col=1)

    fig.update_layout(height=600, width=800, title_text="Rune Price, USD $")
    fig.show()


def _rune_supply_tracker_plot(result_df: pd.DataFrame):

    rune_supply = []
    rune_inflation = []
    rune_deflation = []

    for step in result_df['rune_supply_tracker']:

        rune_supply.append(step.rune_supply)
        rune_inflation.append(step.rune_lend_inflation)
        rune_deflation.append(step.rune_save_deflation)

    fig = make_subplots(
        rows=3, cols=1,
        #specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        vertical_spacing=0.2,
        subplot_titles=("Total Rune Supply",
                        "Rune Minted (Lending), cumulative",
                        "Rune Burned (Savings), cumulative"
                        )
    )

    # Get cumulative values
    inflation = pd.Series(rune_inflation).cumsum()
    deflation = pd.Series(rune_deflation).cumsum()

    # Total Rune Supply
    fig.add_trace(go.Scatter(y=rune_supply,  name='Rune Supply'),
                  row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="Rune", row=1, col=1)
    # Rune Inflation
    fig.add_trace(go.Scatter(y=inflation, name='Rune Minted (Lending)'),
                  row=2, col=1)
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="Rune", row=2, col=1)

    # Rune Deflation
    fig.add_trace(go.Scatter(y=deflation, name='Rune Burned (Savings)'),
                  row=3, col=1)
    fig.update_xaxes(title_text="Step (block)", row=3, col=1)
    fig.update_yaxes(title_text="Rune", row=3, col=1)

    fig.update_layout(height=600, width=800,
                      title_text="Rune Supply Tracker")

    fig.show()


def plot_results(result_df: pd.DataFrame):

    _rune_supply_tracker_plot(result_df)
    _thor_usd_pool_plot(result_df)
    _lend_module_plot(result_df)
    _user_CR_plot(result_df)
    _btc_pool_plot(result_df)
    _thor_usd_vault_plot(result_df)
